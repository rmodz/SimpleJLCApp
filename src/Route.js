import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  AsyncStorage,
  ActivityIndicator,
  StatusBar,
  PixelRatio
} from "react-native";
import {
  Scene,
  Router,
  Actions,
  Reducer,
  ActionConst,
  Overlay,
  Tabs,
  Modal,
  Drawer,
  Stack,
  Lightbox
} from "react-native-router-flux";
import { iconsMap, iconsLoaded } from "./Provider/Icons";
import { LoaderList } from "./Provider/Loader";

import Home from "./Pages/Home";
import Dictionary from "./Pages/Dictionary";
import Question from "./Pages/Question";
import Learn from "./Pages/Learn";
import DrawerComponent from "./Component/DrawerComponent";
// import Icon from "react-native-vector-icons";
import Ionicons from "react-native-vector-icons/Ionicons";
// import FontAwesome from "react-native-vector-icons/FontAwesome";

const iconBurger = () => <Ionicons color="#FFF" name="md-menu" style={{margin: 5}} size={30} />;
const iconAdd = () => <Ionicons color="#FFF" name="md-add" style={{margin: 5}} size={30} />;

//Create a dedicated class that will manage the tabBar icon
class TabIcon extends Component {
  render() {
    var color = this.props.focused ? "#d32f2f" : "#424242";
    console.log(color);
    return (
      <View style={styles.tabIcon}>
        <Icon
          style={{ color: color }}
          name={this.props.iconName || "circle"}
          size={21}
        />
        <Text style={{ color: color, fontSize: 12 }}>{this.props.title}</Text>
      </View>
    );
  }
}

class Route extends Component {
  componentDidMount() {
    iconsLoaded.then(()=>{
        const _ = this;
      // AsyncStorage.getItem("users").then(res => {
      //   if (res) {
      //     console.log("login");
      //     _.setState({ isLogin: true, loading: false });
      //   } else {
      //     console.log("not login");
      //     _.setState({ loading: false });
      //   }
      // });
      _.setState({ loading: false });
    })
  }
  constructor(props) {
    super(props);
    this.state = {
      isLogin: false,
      loading: true
    };
  }

  render() {
    if (this.state.loading) {
      return (
        <View>
          <LoaderList />
        </View>
      );
    }else{
      return <View style={{ flex: 1 }}>
        <StatusBar backgroundColor="#ff82fc" barStyle="dark-content" />
        <Router navigationBarStyle={styles.navBar} titleStyle={styles.navBarTitle} barButtonTextStyle={styles.barButtonTextStyle} barButtonIconStyle={styles.barButtonIconStyle}>
          <Stack key="root">
            <Drawer hideNavBar initial key="drawer" contentComponent={DrawerComponent} drawerIcon={iconBurger} drawerWidth={300}>
              <Scene key="home" title="Kotoba/Kosakata" component={Home}/>
              <Scene key="dict" title="Huruf Jepang" component={Dictionary} />
              <Scene key="quest" title="Pertanyaan Jepang" component={Question} />
              <Scene key="learn" title="List Pelajaran" component={Learn} />
            </Drawer>
          </Stack>
        </Router>
      </View>;
    }
  }
}

const styles = StyleSheet.create({
  tabBar: {
    borderTopColor: "darkgrey",
    borderTopWidth: 1 / PixelRatio.get(),
    backgroundColor: "ghostwhite",
    opacity: 0.98
  },
  tabIcon: {
    flex: 1,
    flexDirection: "column",
    alignItems: "center",
    alignSelf: "center",
    justifyContent: "center"
  },
  navBar: {
    backgroundColor: "#ff82fc"
  },
  navBarTitle: {
    color: "#FFFFFF"
  },
  barButtonTextStyle: {
    color: "#FFFFFF"
  },
  barButtonIconStyle: {
    tintColor: "rgb(255,255,255)"
  }
});

export default Route;
