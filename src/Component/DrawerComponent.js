import React from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  Text,
  View,
  ViewPropTypes,
  TouchableOpacity
} from "react-native";
import Button from 'react-native-button';
import { Actions } from 'react-native-router-flux';
import Ionicons from "react-native-vector-icons/Ionicons";

class DrawerComponent extends React.Component {
  static propTypes = {
    name: PropTypes.string,
    sceneStyle: ViewPropTypes.style,
    title: PropTypes.string,
  }

  static contextTypes = {
    drawer: PropTypes.object,
  }
  
  render() {
    return <View style={styles.container}>
        <View style={styles.header}>
          <Text>Drawer Content</Text>
        </View>

        <View style={styles.list}>
          <TouchableOpacity style={styles.btnMenu} onPress={Actions.home}>
            <Text>
              <Ionicons name="md-home" size={20}/> Home
            </Text>
          </TouchableOpacity>
        </View>

        <View style={styles.list}>
          <TouchableOpacity style={styles.btnMenu} onPress={Actions.dict}>
            <Text>
              <Ionicons name="md-home" size={20}/> Huruf Jepang
            </Text>
          </TouchableOpacity>
        </View>

        <View style={styles.list}>
          <TouchableOpacity style={styles.btnMenu} onPress={Actions.quest}>
            <Text>
              <Ionicons name="md-home" size={20}/> Pertanyaan Jepang
            </Text>
          </TouchableOpacity>
        </View>

        <View style={styles.list}>
          <TouchableOpacity style={styles.btnMenu} onPress={Actions.learn}>
            <Text>
              <Ionicons name="md-home" size={20}/> List Pelajaran
            </Text>
          </TouchableOpacity>
        </View>
      </View>;
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "flex-start",
    marginTop: 10,
    // justifyContent: "space-evenly",
    backgroundColor: "transparent"
  },
  header: {
    flex: 0,
    height: 150,
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
    borderBottomWidth: 1,
    padding: 0
  },
  btnMenu: {
    // color: '#ff82fc',
    paddingRight: 20,
    justifyContent: "center",
    alignItems: "flex-start",
    width: "100%",
    height: "100%"
  },
  list: {
    borderBottomWidth: 1,
    paddingTop: 5,
    paddingBottom: 5,
    width: "100%",
    height: 50
  }
});

export default DrawerComponent;
