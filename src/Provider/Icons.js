import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

// define your suffixes by yourself..
// here we use active, big, small, very-big..
const replaceSuffixPattern = /--(active|big|small|very-big)/g;
const icons = {
  "ios-home": [30, "#bbb"],
  "ios-home--big": [50, "#bbb"],

  "ios-home--active": [30, "#fff"],
  "ios-home--active--big": [50, "#fff"],
  "ios-home--active--very-big": [100, "#fff"],

  "ios-star": [30, "#bbb"],
  "ios-star--active": [30, "#fff"],

  book: [30, "#bbb"],
  "book--active": [30, "#fff"],

  "ios-compass": [30, "#bbb"],
  "ios-compass--active": [30, "#fff"],

  "menu": [30, "#FFF"],
  "menu--active": [30, "#BBB"],

  bookmark: [30, "#bbb"],
  "bookmark--active": [30, "#fff"],

  "ios-settings": [30, "#b71c1c"],
  "ios-settings--active": [30, "#fff"],

  "ios-book-outline": [30, "#bbb"],
  "ios-book-outline--active": [30, "#fff"],

  "md-menu": [30, "#000"],
  "md-menu--active": [30, "#bbb"],

  // Use other Icon provider, see the logic at L39
  facebook: [30, "#bbb", FontAwesome],
  "facebook--active": [30, "#fff", FontAwesome],


};

const defaultIconProvider = Ionicons;

let iconsMap = {};
let iconsLoaded = new Promise((resolve, reject) => {
  new Promise.all(
    Object.keys(icons).map(iconName => {
      const Provider = icons[iconName][2] || defaultIconProvider; // Ionicons
      return Provider.getImageSource(
        iconName.replace(replaceSuffixPattern, ''),
        icons[iconName][0],
        icons[iconName][1]
      )
    })
  ).then(sources => {
    Object.keys(icons)
      .forEach((iconName, idx) => iconsMap[iconName] = sources[idx])

    // Call resolve (and we are done)
    resolve(true);
  })
});

export {
    iconsMap,
    iconsLoaded
};