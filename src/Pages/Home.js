//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Dimensions } from 'react-native';
import SplashScreen from "react-native-splash-screen";
import Ionicons from "react-native-vector-icons/Ionicons";
import { TabViewAnimated, TabBar, SceneMap } from 'react-native-tab-view';
import { Actions } from 'react-native-router-flux';
import Hiragana from './Hiragana';
import Katakana from './Katakana';

const initialLayout = {
  height: 0,
  width: Dimensions.get('window').width,
};

// create a component
class Home extends Component {
  state = {
    index: 0,
    routes: [
      { key: "first", title: "Japan-Indonesia" },
      { key: "second", title: "Indonesia-Japan" }
    ]
  };

  _handleIndexChange = index => this.setState({ index });

  _renderHeader = props => (
    <TabBar
      style={styles.tabCustom}
      labelStyle={styles.tabLabel}
      indicatorStyle={styles.indicator}
      {...props}
    />
  );

  _renderScene = SceneMap({
    first: Hiragana,
    second: Katakana
  });

  componentWillMount() {
    Actions.refresh({ right: this.renderRightButton });
    SplashScreen.hide();
  }

  renderRightButton = () => {
    return (
      <TouchableOpacity onPress={() => this.handleIconTouch()}>
        <Ionicons
          color="#FFF"
          name="md-add"
          style={{ margin: 5, marginRight: 20 }}
          size={30}
        />
      </TouchableOpacity>
    );
  };

  handleIconTouch = () => {
    console.log("Touched!");
  };

  render() {
    return (
      // <View style={styles.container}>
      //   <Text>Home</Text>
      // </View>
      <TabViewAnimated
        navigationState={this.state}
        renderScene={this._renderScene}
        renderHeader={this._renderHeader}
        onIndexChange={this._handleIndexChange}
        initialLayout={initialLayout}
        useNativeDriver
      />
    );
  }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        // padding: 15,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFF',
    },
    tabCustom: {
      backgroundColor: '#ffffff',
      shadowOffset: {height: 0},
      shadowOpacity: 0,
      shadowRadius: 0,
      elevation: 0
    },
    tabLabel: {
      color: '#ff82fc'
    },
    indicator: {
      backgroundColor: '#ff82fc'
    }
});

//make this component available to the app
export default Home;
