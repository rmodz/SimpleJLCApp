import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import SearchInput, { createFilter } from 'react-native-search-filter';

export default class Hiragana extends Component {
  render() {
    return (
      <View style={styles.container}>
        <SearchInput 
          onChangeText={(term) => { }} 
          style={styles.searchInput}
          placeholder="Type a message to search"
          />
        <Text> Hiragana Page </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    backgroundColor: '#FFF',
  },
  searchInput:{
    marginBottom: 10,
    borderColor: '#CCC',
    borderBottomWidth: 1
  }
});
