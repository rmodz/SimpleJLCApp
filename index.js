import { AppRegistry } from 'react-native';
import App from './App';
import Route from "./src/Route";

AppRegistry.registerComponent("JLCApp", () => Route);
